#include <iostream>
#include "chip8.h"
#include <chrono>
#include <ncurses.h>
#include "plateform.h"
#include "chip8Study/chip8Study.h"
#include <thread>

void attendreEspace() {
    initscr();
    cbreak(); // Désactive le buffering de ligne
    noecho(); // Désactive l'affichage des caractères tapés
    keypad(stdscr, TRUE); // Permet l'utilisation des touches spéciales

    refresh(); // Affiche le message

    getch(); // Attend que l'utilisateur appuie sur une touche
    endwin(); // Restaure les paramètres du terminal
}

int main(int argc, char **argv) {
     int videoScale = 20;
    float cycleDelay = 1;
    chip8 chip8;
    chip8Study chip8Study;
    for (int i = 0; i < argc; ++i) {
        std::cout << "Argument " << i << ": " << argv[1] << std::endl;
    }

    std::string romPath = "../../src/roms/";
    romPath += argv[1];
    romPath += ".ch8";
    char const *romFilename = romPath.c_str();

    platform platform("CHIP-8 Emulator", VIDEO_WIDTH * videoScale, VIDEO_HEIGHT * videoScale, VIDEO_WIDTH,
                      VIDEO_HEIGHT);

    chip8.LoadROM(romFilename);

    int videoPitch = sizeof(chip8.video[0]) * VIDEO_WIDTH;

    auto lastCycleTime = std::chrono::high_resolution_clock::now();
    bool quit = false;

    while (!quit) {
        quit = platform::ProcessInput(chip8.keypad);

        auto currentTime = std::chrono::high_resolution_clock::now();
        float dt = std::chrono::duration<float, std::chrono::milliseconds::period>(currentTime - lastCycleTime).count();


        if (dt > cycleDelay) {
            lastCycleTime = currentTime;

                chip8Study.chip8 = chip8;
                chip8Study.currentStackInformation();
                attendreEspace();

            chip8.Cycle();
            platform.Update(chip8.video, videoPitch);
        }

    }
    std::cout << "EXIT" << std::endl;
    return 0;
}
