#include <iostream>
#include "chip8.h"
#include <chrono>
#include "plateform.h"

int main(int argc, char **argv) {


    int videoScale = 20;
    float cycleDelay = 1;
    chip8 chip8;
    char const *romFilename = "/Users/r.godin/CLionProjects/SDL2_Basic_Setup/src/tetris.ch8";

    platform platform("CHIP-8 Emulator", VIDEO_WIDTH * videoScale, VIDEO_HEIGHT * videoScale, VIDEO_WIDTH,
                      VIDEO_HEIGHT);


    chip8.LoadROM(romFilename);

    int videoPitch = sizeof(chip8.video[0]) * VIDEO_WIDTH;

    auto lastCycleTime = std::chrono::high_resolution_clock::now();
    bool quit = false;

    while (!quit) {
        quit = platform::ProcessInput(chip8.keypad);

        auto currentTime = std::chrono::high_resolution_clock::now();
        float dt = std::chrono::duration<float, std::chrono::milliseconds::period>(currentTime - lastCycleTime).count();

        if (dt > cycleDelay) {
            std::cout << "Cycle" << std::endl;
            lastCycleTime = currentTime;

            chip8.Cycle();

            platform.Update(chip8.video, videoPitch);
        }
    }
    std::cout << "EXIT" << std::endl;
    return 0;
}
