//
// Created by Rémy GODIN on 03/07/2024.
//

#ifndef chip8Study_H
#define chip8Study_H
#include <cstdint>
#include "../chip8.h"

class chip8Study {
public:
    uint16_t opcode{};
    uint16_t pc{};

    chip8Study();

    chip8 chip8;

    void fakeCycle();

    void whatOpcodeIsIt(uint16_t opcode);

    void currentStackInformation();

    void showStack();

    // ADD INFORMATION FOR FUNCTION
    void OP_00E0();

    void OP_00EE();

    void OP_1nnn();

    void OP_2nnn();

    void OP_3xkk();

    void OP_4xkk();

    void OP_5xy0();

    void OP_6xkk();

    void OP_7xkk();

    void OP_8xy0();

    void OP_8xy1();

    void OP_8xy2();

    void OP_8xy3();

    void OP_8xy4();

    void OP_8xy5();

    void OP_8xy6();

    void OP_8xy7();

    void OP_8xyE();

    void OP_9xy0();

    void OP_Annn();

    void OP_Bnnn();

    void OP_Cxkk();

    void OP_Dxyn();

    void OP_Ex9E();

    void OP_ExA1();

    void OP_Fx07();

    void OP_Fx0A();

    void OP_Fx15();

    void OP_Fx18();

    void OP_Fx1E();

    void OP_Fx29();

    void OP_Fx33();

    void OP_Fx55();

    void OP_Fx65();

    void Table0();

    void Table8();

    void TableE();

    void TableF();

    void OP_NULL();

    typedef void (chip8Study::*chip8StudyFunc)();

    chip8StudyFunc table[0xF + 1]{};
    chip8StudyFunc table0[0xE + 1]{};
    chip8StudyFunc table8[0xE + 1]{};
    chip8StudyFunc tableE[0xE + 1]{};
    chip8StudyFunc tableF[0x65 + 1]{};
};


#endif //chip8Study_H
