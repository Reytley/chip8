//
// Created by Rémy GODIN on 03/07/2024.
//

#include "chip8Study.h"
#include <fstream>
#include <iostream>
#include <cstdint>
#include <sstream>
#include <iomanip> // Pour std::hex et std::setw
#include <chrono>

chip8Study::chip8Study() {
    table[0x0] = &chip8Study::Table0;
    table[0x1] = &chip8Study::OP_1nnn;
    table[0x2] = &chip8Study::OP_2nnn;
    table[0x3] = &chip8Study::OP_3xkk;
    table[0x4] = &chip8Study::OP_4xkk;
    table[0x5] = &chip8Study::OP_5xy0;
    table[0x6] = &chip8Study::OP_6xkk;
    table[0x7] = &chip8Study::OP_7xkk;
    table[0x8] = &chip8Study::Table8;
    table[0x9] = &chip8Study::OP_9xy0;
    table[0xA] = &chip8Study::OP_Annn;
    table[0xB] = &chip8Study::OP_Bnnn;
    table[0xC] = &chip8Study::OP_Cxkk;
    table[0xD] = &chip8Study::OP_Dxyn;
    table[0xE] = &chip8Study::TableE;
    table[0xF] = &chip8Study::TableF;

    table0[0x0] = &chip8Study::OP_00E0;
    table0[0xE] = &chip8Study::OP_00EE;

    table8[0x0] = &chip8Study::OP_8xy0;
    table8[0x1] = &chip8Study::OP_8xy1;
    table8[0x2] = &chip8Study::OP_8xy2;
    table8[0x3] = &chip8Study::OP_8xy3;
    table8[0x4] = &chip8Study::OP_8xy4;
    table8[0x5] = &chip8Study::OP_8xy5;
    table8[0x6] = &chip8Study::OP_8xy6;
    table8[0x7] = &chip8Study::OP_8xy7;
    table8[0xE] = &chip8Study::OP_8xyE;

    tableE[0x1] = &chip8Study::OP_ExA1;
    tableE[0xE] = &chip8Study::OP_Ex9E;

    tableF[0x07] = &chip8Study::OP_Fx07;
    tableF[0x0A] = &chip8Study::OP_Fx0A;
    tableF[0x15] = &chip8Study::OP_Fx15;
    tableF[0x18] = &chip8Study::OP_Fx18;
    tableF[0x1E] = &chip8Study::OP_Fx1E;
    tableF[0x29] = &chip8Study::OP_Fx29;
    tableF[0x33] = &chip8Study::OP_Fx33;
    tableF[0x55] = &chip8Study::OP_Fx55;
    tableF[0x65] = &chip8Study::OP_Fx65;
}

void chip8Study::whatOpcodeIsIt(uint16_t opcode) {
    (this->*table[(opcode & 0xF000) >> 12])();
}

void chip8Study::currentStackInformation() {
    this->showStack();
    this->fakeCycle();
    std::cout << " --------- memory[$" << std::hex << this->pc - 2
            << std::hex << "] -> OPCODE: $" << std::hex << this->opcode
            << std::hex << " || PC: $" << std::hex << this->pc
            //<< std::hex << "|| DECODE OPCODE: $" <<  std::hex << ( (chip8.opcode & 0xF000u) >> 12u)
            << std::hex << " || INSTRUCTION: ";
    this->whatOpcodeIsIt(this->opcode);
}


void chip8Study::fakeCycle() {
    this->opcode = chip8.memory[chip8.pc] << 8u | chip8.memory[chip8.pc + 1];
    this->pc = chip8.pc + 2;
}

void chip8Study::showStack() {
    std::cout << " ---------" << std::endl;
    for (int i = 0; i < 16; ++i) {
        // Affichage en hexadécimal avec 4 chiffres pour chaque valeur
        std::cout << "|" << std::setw(2) << std::setfill('0') << i << "|0x"
                << std::hex << std::setw(4) << std::setfill('0') << chip8.stack[i] << std::dec;
        if (i == chip8.sp) {
            std::cout << " <- SP";
        }
        std::cout << "" << std::endl;
    }
}


/*
00E0 - CLS
Clear the display.
*/
void chip8Study::OP_00E0() {
    std::cout << "OP_00E0 Clear the display->  memset(video, 0, sizeof(video));" << std::endl;
}

/*
00EE - RET
Return from a subroutine.

The interpreter sets the program counter to the address at the top of the stack, then subtracts 1 from the stack pointer.
*/
void chip8Study::OP_00EE() {
    std::cout << "OP_00EE -> --sp | " << std::hex << "pc = stack[sp/" << std::hex << this->chip8.sp - 1 << std::hex <<
            "] " << std::endl;
}

/*
1nnn - JP addr
Jump to location nnn.

The interpreter sets the program counter to nnn.
*/
void chip8Study::OP_1nnn() {
    uint16_t address = opcode & 0x0FFFu;

    std::cout << "OP_1nnn ->" << std::hex << "pc = address/" << std::hex << address << std::endl;
}

/*
2nnn - CALL addr
Call subroutine at nnn.

The interpreter increments the stack pointer, then puts the current PC on the top of the stack. The PC is then set to nnn.
*/
void chip8Study::OP_2nnn() {
    uint16_t address = this->opcode & 0x0FFFu;

    std::cout << "OP_2nnn -> stack[sp/" << std::hex << static_cast<int>(this->chip8.sp) << std::hex << "] = $"
            << std::hex << this->pc
            << std::hex << " | PC = $" << std::hex << address
            << std::hex << " | SP +1"
            << std::endl;
}

/*
3xkk - SE Vx, byte
Skip next instruction if Vx = kk.

The interpreter compares register Vx to kk, and if they are equal, increments the program counter by 2.
*/
void chip8Study::OP_3xkk() {
    uint8_t Vx = (this->opcode & 0x0F00u) >> 8u;
    uint8_t byte = this->opcode & 0x00FFu;

    std::cout << "OP_3xkk ->  if (registers[Vx/" << std::hex << (int) Vx << std::hex << "] == byte)";
    std::cout << "(" << std::hex << (int) this->chip8.registers[Vx] << std::hex << " == " << std::hex << (int) byte <<
            std::hex << ") { ";
    if (this->chip8.registers[Vx] == byte) {
        std::cout << "pc += 2; }" << std::endl;
    } else {
        std::cout << "no enter to condition }" << std::endl;
    }
}

/*
4xkk - SNE Vx, byte
Skip next instruction if Vx != kk.

The interpreter compares register Vx to kk, and if they are not equal, increments the program counter by 2.
*/
void chip8Study::OP_4xkk() {
    uint8_t Vx = (this->opcode & 0x0F00u) >> 8u;
    uint8_t byte = this->opcode & 0x00FFu;

    std::cout << "OP_4xkk ->  if (registers[Vx/" << std::hex << (int) Vx << std::hex << "] != byte)";
    std::cout << "(" << std::hex << (int) this->chip8.registers[Vx] << std::hex << " != " << std::hex << (int) byte <<
            std::hex << ") { ";
    if (this->chip8.registers[Vx] == byte) {
        std::cout << "pc += 2; }" << std::endl;
    } else {
        std::cout << "no enter to condition }" << std::endl;
    }
}

/*
5xy0 - SE Vx, Vy
Skip next instruction if Vx = Vy.

The interpreter compares register Vx to register Vy, and if they are equal, increments the program counter by 2.
*/
void chip8Study::OP_5xy0() {
    uint8_t Vx = (opcode & 0x0F00u) >> 8u;
    uint8_t Vy = (opcode & 0x00F0u) >> 4u;

    std::cout << "OP_5xy0 ->  if (registers[Vx/" << std::hex << (int) Vx << std::hex << "] == registers[Vy/" << std::hex
            << (int) Vy << std::hex << "])";
    std::cout << "(" << std::hex << (int) this->chip8.registers[Vx] << std::hex << " == " << std::hex << (int) this->
            chip8.registers[Vy] << std::hex << ") { ";
    if (this->chip8.registers[Vx] == this->chip8.registers[Vy]) {
        std::cout << "pc += 2; }" << std::endl;
    } else {
        std::cout << "no enter to condition }" << std::endl;
    }
}

/*
6xkk - LD Vx, byte
Set Vx = kk.

The interpreter puts the value kk into register Vx.
 */
void chip8Study::OP_6xkk() {
    uint8_t Vx = (this->opcode & 0x0F00u) >> 8u;
    uint8_t kk = this->opcode & 0x00FFu;

    std::cout << "OP_6xkk -> registers[" << std::hex << (int) Vx << std::hex << "] = $" << std::hex << (int) kk <<
            std::endl;
}

/*
7xkk - ADD Vx, byte
Set Vx = Vx + kk.

Adds the value kk to the value of register Vx, then stores the result in Vx.
*/
void chip8Study::OP_7xkk() {
    uint8_t Vx = (this->opcode & 0x0F00u) >> 8u;
    uint8_t byte = this->opcode & 0x00FFu;

    std::cout << "OP_7xkk ->" << std::hex << "registers[Vx/" << std::hex << (int) Vx
            << std::hex << "] += byte/" << std::hex << (int) byte << std::hex << ";" << std::endl;
}

/*
8xy0 - LD Vx, Vy
Set Vx = Vy.

Stores the value of register Vy in register Vx.
*/
void chip8Study::OP_8xy0() {
    uint8_t Vx = (opcode & 0x0F00u) >> 8u;
    uint8_t Vy = (opcode & 0x00F0u) >> 4u;
    std::cout << "OP_8xy0 ->" << std::hex << "registers[Vx/" << std::hex << (int) Vx
            << std::hex << "] = registers[Vy/" << std::hex << (int) Vy << std::hex << "];" << std::endl;
}

/*
8xy1 - OR Vx, Vy
Set Vx = Vx OR Vy.

Performs a bitwise OR on the values of Vx and Vy, then stores the result in Vx.
A bitwise OR compares the corrseponding bits from two values,
and if either bit is 1, then the same bit in the result is also 1. Otherwise, it is 0.
*/
void chip8Study::OP_8xy1() {
    uint8_t Vx = (opcode & 0x0F00u) >> 8u;
    uint8_t Vy = (opcode & 0x00F0u) >> 4u;
    std::cout << "OP_8xy1 ->" << std::hex << "registers[Vx/" << std::hex << (int) Vx
            << std::hex << "] |= registers[Vy/"
            << std::hex << (int) Vy << std::hex << "];" << std::endl;
}

/*
8xy2 - AND Vx, Vy
Set Vx = Vx AND Vy.

Performs a bitwise AND on the values of Vx and Vy, then stores the result in Vx.
A bitwise AND compares the corrseponding bits from two values, and if both bits are 1,
then the same bit in the result is also 1. Otherwise, it is 0.
*/
void chip8Study::OP_8xy2() {
    uint8_t Vx = (opcode & 0x0F00u) >> 8u;
    uint8_t Vy = (opcode & 0x00F0u) >> 4u;
    std::cout << "OP_8xy2 ->" << std::hex << "registers[Vx/" << std::hex << (int) Vx
            << std::hex << "] &= registers[Vy/" << std::hex << (int) Vy << std::hex << "];" << std::endl;
}

/*
8xy3 - XOR Vx, Vy
Set Vx = Vx XOR Vy.

Performs a bitwise exclusive OR on the values of Vx and Vy, then stores the result in Vx.
An exclusive OR compares the corrseponding bits from two values,
and if the bits are not both the same, then the corresponding bit in the result is set to 1. Otherwise, it is 0.

*/
void chip8Study::OP_8xy3() {
    uint8_t Vx = (opcode & 0x0F00u) >> 8u;
    uint8_t Vy = (opcode & 0x00F0u) >> 4u;
    std::cout << "OP_8xy3 ->" << std::hex << "registers[Vx/" << std::hex << (int) Vx
            << std::hex << "] ^= registers[Vy/" << std::hex << (int) Vy << std::hex << "];" << std::endl;
}

/*

8xy4 - ADD Vx, Vy
Set Vx = Vx + Vy, set VF = carry.

The values of Vx and Vy are added together.
If the result is greater than 8 bits (i.e., > 255,) VF is set to 1, otherwise 0.
Only the lowest 8 bits of the result are kept, and stored in Vx.
*/
void chip8Study::OP_8xy4() {
    uint8_t Vx = (opcode & 0x0F00u) >> 8u;
    uint8_t Vy = (opcode & 0x00F0u) >> 4u;
    uint16_t sum = chip8.registers[Vx] + chip8.registers[Vy];

    std::cout << "OP_8xy4 -> Set Vx = Vx/" << (int) Vx << " + Vy/" << std::hex
            << (int) Vy << ", set VF/registers[0xF] = carry/";
    if (sum > 255U) {
        std::cout << "1";
    } else {
        std::cout << "0";
    }
    std::cout << " registers[Vx] = " << std::hex << (int) sum << " & 0xFFu;";
    std::cout << "" << std::endl;
}

/*
8xy5 - SUB Vx, Vy
Set Vx = Vx - Vy, set VF = NOT borrow.

If Vx > Vy, then VF is set to 1, otherwise 0. Then Vy is subtracted from Vx, and the results stored in Vx.
*/
void chip8Study::OP_8xy5() {
    uint8_t Vx = (opcode & 0x0F00u) >> 8u;
    uint8_t Vy = (opcode & 0x00F0u) >> 4u;

    std::cout << "OP_8xy5 -> if (registers[Vx/" << std::hex << (int) Vx << "] > registers[Vy]/" << std::hex << (int) Vy
            << ")";
    std::cout << "(" << std::hex << (int) chip8.registers[Vx] << std::hex << " > " << std::hex << (int) chip8.registers[
        Vy] << std::hex << ") {";
    if (chip8.registers[Vx] > chip8.registers[Vy]) {
        std::cout << "registers[0xF] = 1; }";
    } else {
        std::cout << "} else { registers[0xF] = 0;}";
    }
    std::cout << " registers[Vx] -= registers[Vy];" << std::endl;
}

/*

8xy6 - SHR Vx {, Vy}
Set Vx = Vx SHR 1.

If the least-significant bit of Vx is 1, then VF is set to 1, otherwise 0. Then Vx is divided by 2.
*/
void chip8Study::OP_8xy6() {
    uint8_t Vx = (opcode & 0x0F00u) >> 8u;

    // Save LSB in VF
    std::cout << "OP_8xy6 -> registers[0xF] = (registers[Vx/" << std::hex << (int) Vx << "] & 0x1u)/"
            << std::hex << (int) (chip8.registers[Vx] & 0x1u) << "; registers[Vx/" << std::hex << (int) Vx << "] >>= 1;"
            << std::endl;
}

/*
8xy7 - SUBN Vx, Vy
Set Vx = Vy - Vx, set VF = NOT borrow.

If Vy > Vx, then VF is set to 1, otherwise 0. Then Vx is subtracted from Vy, and the results stored in Vx.
*/
void chip8Study::OP_8xy7() {
    uint8_t Vx = (opcode & 0x0F00u) >> 8u;
    uint8_t Vy = (opcode & 0x00F0u) >> 4u;

    std::cout << "OP_8xy7 -> if (registers[Vy/" << std::hex << (int) Vy << "] > registers[Vx]/" << std::hex << (int) Vx
            << ")";
    std::cout << "(" << std::hex << (int) chip8.registers[Vy] << std::hex << " > " << std::hex << (int) chip8.registers[
        Vx] << std::hex << ") {";
    if (chip8.registers[Vx] > chip8.registers[Vy]) {
        std::cout << "registers[0xF] = 1; }";
    } else {
        std::cout << "} else { registers[0xF] = 0;}";
    }
    std::cout << "cregisters[Vx] = registers[Vy] - registers[Vx];" << std::endl;
}

/*
8xyE - SHL Vx {, Vy}
Set Vx = Vx SHL 1.

If the most-significant bit of Vx is 1, then VF is set to 1, otherwise to 0. Then Vx is multiplied by 2.
*/
void chip8Study::OP_8xyE() {
    uint8_t Vx = (opcode & 0x0F00u) >> 8u;
    std::cout << "OP_8xyE" << std::endl;
    // Save MSB in VF
    std::cout << "registers[0xF] = ((registers[Vx/" << std::hex << Vx << "] & 0x80u) >> 7u)/" << std::hex << (
        (chip8.registers[Vx] & 0x80u) >> 7u) << ";";

    std::cout << "registers[Vx/" << std::hex << Vx << "] <<= 1;" << std::endl;
}

/*
9xy0 - SNE Vx, Vy
Skip next instruction if Vx != Vy.

The values of Vx and Vy are compared, and if they are not equal, the program counter is increased by 2.
*/
void chip8Study::OP_9xy0() {
    std::cout << "OP_9xy0" << std::endl;

    uint8_t Vx = (opcode & 0x0F00u) >> 8u;
    uint8_t Vy = (opcode & 0x00F0u) >> 4u;

    std::cout << "if (registers[Vx/" << std::hex << Vx << "] != registers[Vy/" << std::hex << Vy << "])/ ";
    std::cout << "(" << std::hex << chip8.registers[Vx] << std::hex << " !=" << std::hex << chip8.registers[Vy] << "){"
            << std::endl;

    std::cout << "   pc += 2;  }" << std::endl;
}

/*
Annn - LD I, addr
Set I = nnn.
The value of register I is set to nnn
*/
void chip8Study::OP_Annn() {
    uint16_t address = this->opcode & 0x0FFFu; //address === nnn
    //I === index
    std::cout << "OP_Annn -> index = $" << std::hex << address << std::endl;
}

/*
Bnnn - JP V0, addr
Jump to location nnn + V0.

The program counter is set to nnn plus the value of V0.
*/
void chip8Study::OP_Bnnn() {
    uint16_t address = opcode & 0x0FFFu;
    std::cout << "OP_Bnnn -> pc = registers[0]/ " << std::hex << this->chip8.registers[0] << std::hex << "+ " <<
            std::hex << address << std::hex << ";" << std::endl;
}

/*
Cxkk - RND Vx, byte
Set Vx = random byte AND kk.

The interpreter generates a random number from 0 to 255, which is then ANDed with the value kk.
The results are stored in Vx.
See instruction 8xy2 for more information on AND.
*/
void chip8Study::OP_Cxkk() {
    uint8_t Vx = (opcode & 0x0F00u) >> 8u;
    uint8_t byte = opcode & 0x00FFu;

    std::cout << "OP_Cxkk -> registers[Vx/" << std::hex << Vx << "] = randByte(randGen) & byte/" << std::hex << byte <<
            ";" << std::endl;
}

/*
Dxyn - DRW Vx, Vy, nibble
Display n-byte sprite starting at memory location I at (Vx, Vy), set VF = collision.

The interpreter reads n bytes from memory, starting at the address stored in I.
These bytes are then displayed as sprites on screen at coordinates (Vx, Vy).
Sprites are XORed onto the existing screen.
If this causes any pixels to be erased, VF is set to 1, otherwise it is set to 0.
If the sprite is positioned so part of it is outside the coordinates of the display, it wraps around to the opposite side of the screen.
See instruction 8xy3 for more information on XOR, and section 2.4, Display, for more information on the Chip-8 screen and sprites.
 */
void chip8Study::OP_Dxyn() {
    std::cout << "OP_Dxyn : Display n-byte sprite starting at memory location I at (Vx, Vy), set VF = collision." <<
            std::endl;
}

/*
Ex9E - SKP Vx
Skip next instruction if key with the value of Vx is pressed.

Checks the keyboard, and if the key corresponding to the value of Vx is currently in the down position, PC is increased by 2.
*/
void chip8Study::OP_Ex9E() {
    uint8_t Vx = (opcode & 0x0F00u) >> 8u;
    uint8_t key = chip8.registers[Vx];
    std::cout << "OP_Ex9E -> if (keypad[key/" << std::hex << key << std::hex << "]){pc += 2;}" << std::endl;
}

/*
ExA1 - SKNP Vx
Skip next instruction if key with the value of Vx is not pressed.

Checks the keyboard, and if the key corresponding to the value of Vx is currently in the up position, PC is increased by 2.
*/
void chip8Study::OP_ExA1() {
    uint8_t Vx = (opcode & 0x0F00u) >> 8u;
    uint8_t key = chip8.registers[Vx];
    std::cout << "OP_Ex9E -> if (!keypad[key/" << std::hex << key << std::hex << "]){pc += 2;}" << std::endl;
}

/*
Fx07 - LD Vx, DT
Set Vx = delay timer value.

The value of DT is placed into Vx.
*/
void chip8Study::OP_Fx07() {
    uint8_t Vx = (opcode & 0x0F00u) >> 8u;
    std::cout << "OP_Fx07 ->  registers[Vx/" << std::hex << Vx << std::hex << "] = delayTimer/" << std::hex << chip8.
            delayTimer << std::hex << ";" << std::endl;
}

/*
Fx0A - LD Vx, K
Wait for a key press, store the value of the key in Vx.

All execution stops until a key is pressed, then the value of that key is stored in Vx.
*/
void chip8Study::OP_Fx0A() {
    std::cout << "OP_Fx0A -> Wait for a key press, store the value of the key in Vx." << std::endl;
}

/*
Fx15 - LD DT, Vx
Set delay timer = Vx.

DT is set equal to the value of Vx.
*/
void chip8Study::OP_Fx15() {
    uint8_t Vx = (opcode & 0x0F00u) >> 8u;
    std::cout << "OP_Fx15 delayTimer = registers[Vx/" << std::hex << Vx << std::hex << "];" << std::endl;
}

/*
Fx18 - LD ST, Vx
Set sound timer = Vx.

ST is set equal to the value of Vx.
*/
void chip8Study::OP_Fx18() {
    uint8_t Vx = (opcode & 0x0F00u) >> 8u;
    std::cout << "OP_Fx18 soundTimer = registers[Vx/" << std::hex << Vx << std::hex << "];" << std::endl;
}

/*
Fx1E - ADD I, Vx
Set I = I + Vx.

The values of I and Vx are added, and the results are stored in I.
*/
void chip8Study::OP_Fx1E() {
    uint8_t Vx = (opcode & 0x0F00u) >> 8u;
    std::cout << "OP_Fx1E index += registers[Vx/" << std::hex << Vx << std::hex << "];" << std::endl;
}

/*
Fx29 - LD F, Vx
Set I = location of sprite for digit Vx.

The value of I is set to the location for the hexadecimal sprite corresponding to the value of Vx. See section 2.4,
Display, for more information on the Chip-8 hexadecimal font.

*/
void chip8Study::OP_Fx29() {
    uint8_t Vx = (opcode & 0x0F00u) >> 8u;
    uint8_t digit = chip8.registers[Vx];

    std::cout << "OP_Fx29 ->  index = FONTSET_START_ADDRESS + (5 * digit/" << std::hex << digit << "); index = "
            << std::hex << (FONTSET_START_ADDRESS + (5 * digit)) << std::endl;
}

/*
Fx33 - LD B, Vx
Store BCD representation of Vx in memory locations I, I+1, and I+2.

The interpreter takes the decimal value of Vx, and places the hundreds digit in memory at location in I,
the tens digit at location I+1, and the ones digit at location I+2.
*/
void chip8Study::OP_Fx33() {
    std::cout << "OP_Fx33" << std::endl;
}

/*
Fx55 - LD [I], Vx
Store registers V0 through Vx in memory starting at location I.

The interpreter copies the values of registers V0 through Vx into memory, starting at the address in I.
*/
void chip8Study::OP_Fx55() {
    std::cout << "OP_Fx55 -> Store registers V0 through Vx in memory starting at location I." << std::endl;
}

/*
Fx65 - LD Vx, [I]
Read registers V0 through Vx from memory starting at location I.

The interpreter reads values from memory starting at location I into registers V0 through Vx.
*/
void chip8Study::OP_Fx65() {
    std::cout << "OP_Fx65 -> Read registers V0 through Vx from memory starting at location I." << std::endl;
}

/*
OP_NULL
*/
void chip8Study::OP_NULL() {
    std::cout << "OP_NULL" << std::endl;
}


void chip8Study::Table0() {
    ((*this).*(table0[opcode & 0x000Fu]))();
}

void chip8Study::Table8() {
    ((*this).*(table8[opcode & 0x000Fu]))();
}

void chip8Study::TableE() {
    ((*this).*(tableE[opcode & 0x000Fu]))();
}

void chip8Study::TableF() {
    ((*this).*(tableF[opcode & 0x00FFu]))();
}
